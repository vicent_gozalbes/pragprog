def progress
  0.step(100, 10) { |p| yield p }
end

progress { |percent| puts "#{percent}%" }
