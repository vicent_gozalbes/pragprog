class Integer
  def n_times
    1.upto(self) { |c| yield(c) }
  end
end

5.times do |n|
  puts "#{n} - Hi!"
end
